import model.Node;
import org.apache.commons.lang3.tuple.Pair;

public class FloorAndCeilingOfABST {

    /*
        Given an integer k and a binary search tree, find the floor (less than or equal to) of k,
        and the ceiling (larger than or equal to) of k. If either does not exist, then print them as None.

               8
            /     \
           4       12
          / \     /  \
         2   6   10   14

         getFloorAndCeiling(root, 5) -> (4, 6)
     */

    private FloorAndCeilingOfABST(){
        // utility class
    }

    public static Pair<Integer, Integer> getFloorAndCeiling(Node<Integer> binarySearchTree, Integer searchedValue){
        Node<Integer> currentNode = binarySearchTree;
        while (currentNode.getValue() > searchedValue){
            if(currentNode.getRight() != null && currentNode.getRight().getValue() <= searchedValue){
                currentNode = currentNode.getRight();
            }else{
                currentNode = currentNode.getLeft();
            }
        }
        Integer ceiling = (currentNode.getRight().getValue() != null) ? currentNode.getRight().getValue() : null;
        return Pair.of(currentNode.getValue(), ceiling);
    }
}
