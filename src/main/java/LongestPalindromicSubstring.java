import org.apache.commons.lang3.StringUtils;

public class LongestPalindromicSubstring {

    /*
        A palindrome is a sequence of characters that nreads the same backwards and forwards.
        Given a string, s, find the longest palindromic substring in s
        ex :
        Input : "banana"
        Output : "anana"

        Input : "million"
        Output : "illi"
     */

    private LongestPalindromicSubstring() {
        // utility class
    }

    public static String getLongestPalindromicSubstring(String str) {
        if (StringUtils.isBlank(str)){
            return "";
        }
        int start = 0;
        int end = 0;
        for (int i = 0; i < str.length(); i++) {
            int len1 = expandAroundCenter(str, i, i);
            int len2 = expandAroundCenter(str, i, i + 1);
            int len = Math.max(len1, len2);
            if (len > end - start) {
                start = i - (len - 1) / 2;
                end = i + len / 2;
            }
        }
        return str.substring(start, end + 1);
    }

    private static int expandAroundCenter(String str, int left, int right) {
        while (left >= 0 && right < str.length() && str.charAt(left) == str.charAt(right)) {
            left--;
            right++;
        }
        return right - left - 1;
    }
}
