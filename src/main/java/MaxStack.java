import com.google.common.collect.Lists;

import java.util.List;

public class MaxStack {

    /*
     * Implement a class for a stack that supports all the regular functions (push, pop)
     * and an additional function of max() which returns the maximum element in the stack
     * (return null if the stack is empty). Each method should run in constant time.
     *
     * exemple :
     * MaxStack maxStack = new MaxStack()
     * maxStack.push(1)
     * maxStack.push(2)
     * maxStack.push(3)
     * maxStack.push(2)
     * maxStack.max() -> 3
     */

    private final List<Integer> list;

    public MaxStack() {
        this.list = Lists.newArrayList();
    }

    public void push(int i) {
        this.list.add(0, i);
    }

    public void pop() {
        this.list.remove(0);
    }

    public Integer getMax() {
        if (this.list.isEmpty()) {
            return null;
        }
        return this.list.stream().max(Integer::compare).get();
    }
}
