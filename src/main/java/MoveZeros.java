import com.google.common.collect.Lists;

import java.util.List;

public class MoveZeros {
    /*
        Given an array nums, write a function to move all 0's to the end of it
        while maintaining the relative order of the non-zero elements.

        Exemple :
        Input: [0,1,0,3,12]
        Output: [1,3,12,0,0]
     */

    private MoveZeros() {
        // utility class
    }

    public static List<Integer> moveZeros(List<Integer> numbers) {
        List<Integer> numbersWithZerosAtTheEnd = Lists.newArrayList();
        int numberOfZeros = 0;
        for (Integer currentNumber : numbers) {
            if (currentNumber.equals(0)) {
                numberOfZeros++;
            } else {
                numbersWithZerosAtTheEnd.add(currentNumber);
            }
        }
        // add zeros found
        for (int i = 0; i < numberOfZeros; i++) {
            numbersWithZerosAtTheEnd.add(0);
        }
        return numbersWithZerosAtTheEnd;
    }
}
