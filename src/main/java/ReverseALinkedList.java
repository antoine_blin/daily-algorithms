import com.google.common.collect.Lists;

import java.util.LinkedList;

public class ReverseALinkedList {

    /*
        Given a singly-linked list, reverse the list.
         This can be done iteratively or recursively. Can you get both solutions?

        Example:
        Input: 4 -> 3 -> 2 -> 1 -> 0 -> NULL
        Output: 0 -> 1 -> 2 -> 3 -> 4 -> NULL
     */

    private ReverseALinkedList(){
        // utility class
    }

    public static LinkedList<Integer> getReverseLinkedList(LinkedList<Integer> inputList){
        LinkedList<Integer> reversedList = Lists.newLinkedList();
        for(int i = inputList.size() - 1; i >= 0; i--){
            reversedList.add(inputList.get(i));
        }
        return reversedList;
    }
}
