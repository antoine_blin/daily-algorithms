import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class FirstAndLastIndicesOfAnElementTest {

    @Test
    public void Test1(){
        ArrayList<Integer> inputSortedArray = Lists.newArrayList(1, 3, 3, 5, 7, 8, 9, 9, 9, 15);
        Integer inputTarget = 9;
        Pair<Integer, Integer> expectedFirstAndLastIndices = Pair.of(6, 8);
        Assert.assertEquals(expectedFirstAndLastIndices, FirstAndLastIndicesOfAnElement.getFirstAndLastIndices(inputSortedArray, inputTarget));
    }

    @Test
    public void Test2(){
        ArrayList<Integer> inputSortedArray = Lists.newArrayList(100, 150, 150, 153);
        Integer inputTarget = 150;
        Pair<Integer, Integer> expectedFirstAndLastIndices = Pair.of(1, 2);
        Assert.assertEquals(expectedFirstAndLastIndices, FirstAndLastIndicesOfAnElement.getFirstAndLastIndices(inputSortedArray, inputTarget));
    }

    @Test
    public void Test3(){
        ArrayList<Integer> inputSortedArray = Lists.newArrayList(1,2,3,4,5,6,10);
        Integer inputTarget = 9;
        Pair<Integer, Integer> expectedFirstAndLastIndices = Pair.of(-1, -1);
        Assert.assertEquals(expectedFirstAndLastIndices, FirstAndLastIndicesOfAnElement.getFirstAndLastIndices(inputSortedArray, inputTarget));
    }
}