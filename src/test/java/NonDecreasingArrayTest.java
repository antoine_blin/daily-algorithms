import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class NonDecreasingArrayTest {

    @Test
    public void test1(){
        assertTrue(NonDecreasingArray.isArrayNonDecreasingByModifyingAtMostOneValue(new int[]{13, 4, 7}));
    }

    @Test
    public void test2(){
        assertFalse(NonDecreasingArray.isArrayNonDecreasingByModifyingAtMostOneValue(new int[]{13, 4, 1}));
    }
}