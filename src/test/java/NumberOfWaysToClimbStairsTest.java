import org.junit.Assert;
import org.junit.Test;

public class NumberOfWaysToClimbStairsTest {

    @Test
    public void test1(){
        Assert.assertEquals(5, NumberOfWaysToClimbStairs.getNumberOfWaysToClimbStairs(4));
    }

    @Test
    public void test2(){
        Assert.assertEquals(8, NumberOfWaysToClimbStairs.getNumberOfWaysToClimbStairs(5));
    }
}