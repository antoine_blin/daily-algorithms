import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class SortingAListWith3UniqueNumbersTest {

    @Test
    public void test(){
        List<Integer> inputList = Lists.newArrayList(Arrays.asList(3, 3, 2, 1, 3, 2, 1));
        List<Integer> expectedSortedList = Lists.newArrayList(Arrays.asList(1, 1, 2, 2, 3, 3, 3));
        Assert.assertEquals(expectedSortedList, SortingAListWith3UniqueNumbers.getListSorted(inputList));
    }
}